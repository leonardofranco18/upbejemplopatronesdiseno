/**
 * Created by WINDOWS 8.1 on 17/04/2018.
 */
public class Persona {

    public String nombre;
    public int edad;
    public String nacionalidad;

    public Persona(String nombre, int edad, String nacionalidad){

        this.nombre=nombre;
        this.edad=edad;
        this.nacionalidad=nacionalidad;

    }
    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }
}
