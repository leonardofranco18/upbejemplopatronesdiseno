/**
 * Created by WINDOWS 8.1 on 17/04/2018.
 */
public class Figura {
    public int tamano;
    public String color;
    public String nombre;


    public Figura(int tamano, String color, String nombre){

        this.tamano=tamano;
        this.color=color;
        this.nombre=nombre;
    }

    public int getTamano() {
        return tamano;
    }

    public String getColor() {
        return color;
    }

    public String getNombre() {
        return nombre;
    }
}
